xquery version "2004-draft";
(:: pragma bea:global-element-return element="ns0:auditDetailsRequest" location="../../../UTL_EIPAuditService/Schema/EIPAudit.xsd" ::)

declare namespace ns0 = "http://www.biogenidec.com/eip/services/auditservice";
declare namespace xf = "http://tempuri.org/CMN_Resources/transformation/audit/newauditDetailTransformation/";

declare function xf:newauditDetailTransformation($version as xs:string,
    $updTime as xs:dateTime,
    $message as xs:string,
    $messageType as xs:string,
    $source as xs:string,
    $clientIdentifier as xs:string,
    $biogenTransId as xs:string)
    as element(ns0:auditDetails) {
        <ns0:auditDetails version = "{ $version }">
            <ns0:version1>
                <ns0:biogenTransId>{ $biogenTransId }</ns0:biogenTransId>
                <ns0:updTime>{ $updTime }</ns0:updTime>
                <ns0:message>{ $message }</ns0:message>
                <ns0:messageType>{ $messageType }</ns0:messageType>
                <ns0:source>{ $source }</ns0:source>
                <ns0:clientIdentifier>{ $clientIdentifier }</ns0:clientIdentifier>
            </ns0:version1>
        </ns0:auditDetails>
};

declare variable $version as xs:string external;
declare variable $updTime as xs:dateTime external;
declare variable $message as xs:string external;
declare variable $messageType as xs:string external;
declare variable $source as xs:string external;
declare variable $clientIdentifier as xs:string external;
declare variable $biogenTransId as xs:string external;

xf:newauditDetailTransformation($version,
    $updTime,
    $message,
    $messageType,
    $source,
    $clientIdentifier,
    $biogenTransId)