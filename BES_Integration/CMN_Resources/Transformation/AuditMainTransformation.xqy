xquery version "2004-draft";
(:: pragma bea:global-element-return element="ns0:auditMainRequest" location="../../../UTL_EIPAuditService/Schema/EIPAudit.xsd" ::)

declare namespace ns0 = "http://www.biogenidec.com/eip/services/auditservice";
declare namespace xf = "http://tempuri.org/CMN_Resources/transformation/audit/newauditMainTransformation/";

declare function xf:newauditMainTransformation($version as xs:string,
    $BiogenTransId as xs:string,
    $StartTime as xs:dateTime,
    $ClientId as xs:string,
    $ClientTransactionId as xs:string,
    $ServiceName as xs:string)
    as element(ns0:auditMain) {
        <ns0:auditMain version = "{ $version }">
            <ns0:version1>
                <ns0:biogenTransId>{ $BiogenTransId }</ns0:biogenTransId>
                <ns0:startTime>{ $StartTime }</ns0:startTime>
                <ns0:clientIdentifier>{ $ClientId }</ns0:clientIdentifier>
                <ns0:clientTransactionID>{ $ClientTransactionId }</ns0:clientTransactionID>
                <ns0:serviceName>{ $ServiceName }</ns0:serviceName>
            </ns0:version1>
        </ns0:auditMain>
};

declare variable $version as xs:string external;
declare variable $BiogenTransId as xs:string external;
declare variable $StartTime as xs:dateTime external;
declare variable $ClientId as xs:string external;
declare variable $ClientTransactionId as xs:string external;
declare variable $ServiceName as xs:string external;

xf:newauditMainTransformation($version,
    $BiogenTransId,
    $StartTime,
    $ClientId,
    $ClientTransactionId,
    $ServiceName)