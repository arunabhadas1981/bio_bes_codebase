xquery version "2004-draft";
(:: pragma  parameter="$payload" type="anyType" ::)
(:: pragma bea:global-element-parameter parameter="$faultServiceRequest" element="ns0:submitRequest" location="../../Schema/faultResponse/FaultResponse.xsd" ::)
(:: pragma bea:global-element-return element="ns2:notificationRequest" location="../../Schema/notification/Notification.xsd" ::)

declare namespace ns2 = "http://www.biogenidec.com/eip/services/notificationservice";
declare namespace ns1 = "http://www.biogenidec.com/schema/base";
declare namespace ns0 = "http://www.biogenidec.com/eip/services/faultresponseservice";
declare namespace xf = "http://tempuri.org/CMN_Resources/transformation/notification/";

declare function xf:notificationRequestTransformation($payload as xs:string,
    $faultServiceRequest as element(ns0:submitRequest),
    $transId as xs:string,
    $serviceName as xs:string,
    $appId as xs:string,
    $faultMessage as xs:string,
$errorCode as xs:string)
    as element(ns2:notificationRequest) {
        <ns2:notificationRequest>
         <ns2:version1>
            <ns2:appId>{ $appId }</ns2:appId>
            <ns2:biogenTransId>{$transId}</ns2:biogenTransId>
            <ns2:faultedService>{ $serviceName }</ns2:faultedService>
            <ns2:faultMessage>{$faultMessage}</ns2:faultMessage>
           
	      <ns2:errorCode>{ $errorCode }</ns2:errorCode>
		<ns2:errorDesc>{$faultMessage}</ns2:errorDesc>
		 <ns2:notificationTime>{ fn:current-dateTime() }</ns2:notificationTime>
            <ns2:payload>{ fn:concat('<![CDATA[',$payload,']]>')}</ns2:payload>
    		
            
            {
                for $message in $faultServiceRequest/ns0:messages/ns0:message
                return
                    <ns2:exceptionAttribute>
                        <ns2:name>{ data($message/ns0:messageCode) }</ns2:name>
                        <ns2:value>{ data($message/ns0:messageDescription) }</ns2:value>
                    </ns2:exceptionAttribute>
            }
             </ns2:version1>
        </ns2:notificationRequest>
};

declare variable $payload as xs:string external;
declare variable $faultServiceRequest as element(ns0:submitRequest) external;
declare variable $transId as xs:string external;
declare variable $serviceName as xs:string external;
declare variable $appId as xs:string external;
declare variable $faultMessage as xs:string external;
declare variable $errorCode as xs:string external;


xf:notificationRequestTransformation($payload,
    $faultServiceRequest,
    $transId,
    $serviceName,
    $appId,
    $faultMessage,
     $errorCode)