xquery version "2004-draft";
(:: pragma bea:global-element-return element="ns0:loggingRequest" location="../WSDL/Logging.wsdl" ::)

declare namespace ns0 = "http://www.biogen.com/eip/services/loggingservice";
declare namespace xf = "http://tempuri.org/CMN_Resources/Transformation/LoggingReq/";

declare function xf:LoggingReq($BiogenTransId as xs:string,
    $appId as xs:string,
    $logLevel as xs:string,
    $messageType as xs:string,
    $message as xs:string,
    $serviceInvokerName as xs:string,
    $needPayloadEncryption as xs:string,
    $entityAttribute1 as xs:string,
    $entityAttribute2 as xs:string,
    $entityAttribute3 as xs:string,
    $updTime as xs:string)
    as element(ns0:loggingRequest) {
        <ns0:loggingRequest>
            <ns0:version1>
                <ns0:biogenTransId>{ $BiogenTransId }</ns0:biogenTransId>
                <ns0:appId>{ $appId }</ns0:appId>
                <ns0:logLevel>{ $logLevel }</ns0:logLevel>
                <ns0:messageType>{ $messageType }</ns0:messageType>
                <ns0:message>{ $message }</ns0:message>
                <ns0:serviceInvokerName>{ $serviceInvokerName }</ns0:serviceInvokerName>
                <ns0:needPayloadEncryption>{ $needPayloadEncryption }</ns0:needPayloadEncryption>
                <ns0:entityAttributes>
                    <ns0:entityAttribute>
                        <ns0:value>{ $entityAttribute1 }</ns0:value>
                    </ns0:entityAttribute>
                    <ns0:entityAttribute>
                        <ns0:value>{ $entityAttribute2 }</ns0:value>
                    </ns0:entityAttribute>
                    <ns0:entityAttribute>
                        <ns0:value>{ $entityAttribute3 }</ns0:value>
                    </ns0:entityAttribute>
                </ns0:entityAttributes>
                <ns0:updTime>{$updTime}</ns0:updTime>
            </ns0:version1>
        </ns0:loggingRequest>
};

declare variable $BiogenTransId as xs:string external;
declare variable $appId as xs:string external;
declare variable $logLevel as xs:string external;
declare variable $messageType as xs:string external;
declare variable $message as xs:string external;
declare variable $serviceInvokerName as xs:string external;
declare variable $needPayloadEncryption as xs:string external;
declare variable $entityAttribute1 as xs:string external;
declare variable $entityAttribute2 as xs:string external;
declare variable $entityAttribute3 as xs:string external;
declare variable $updTime as xs:string external;

xf:LoggingReq($BiogenTransId,
    $appId,
    $logLevel,
    $messageType,
    $message,
    $serviceInvokerName,
    $needPayloadEncryption,
    $entityAttribute1,
    $entityAttribute2,
    $entityAttribute3,
    $updTime)