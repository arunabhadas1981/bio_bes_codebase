xquery version "2004-draft" encoding "Cp1252";
(:: pragma  parameter="$request" type="xs:anyType" ::)
(:: pragma bea:global-element-return element="ns0:submitRequest" location="../../Schema/faultResponse/FaultResponse.xsd" ::)

declare namespace ns0 = "http://www.biogenidec.com/eip/services/responseprocess";
declare namespace ns1 = "http://www.biogenidec.com/schema/base";
declare namespace ns2 = "http://schemas.xmlsoap.org/soap/envelope";

declare function faultResponse_RequestTransformation($request as element(*),
    $version as xs:string,$defaultFaultCode as xs:string)
     as element(ns0:submitRequest) {
        <ns0:submitRequest version = "{ $version }">
               {
               if(fn:exists($request/*:errorcode)) then
               (
            <ns0:messages>
                                <ns0:message>
                                   <ns0:messageCode>{ data($request/*:errorcode) }</ns0:messageCode>
                                   <ns0:messageDescription>{ data($request/*:message) }</ns0:messageDescription>
                               </ns0:message>
                                            {
                                              for $exceptionAttribute in $request/*:exceptionAttribute
                                              return
                                                            (
                                                            <ns0:message>
                                                            <ns0:messageCode>{ data($exceptionAttribute/*:Name) }</ns0:messageCode>
                                                            <ns0:messageDescription>{ data($exceptionAttribute/*:Value) }</ns0:messageDescription>
                                                            </ns0:message>
                                                            )
                                             }
                                             
            </ns0:messages>
            )
            else
            (
            if(fn:local-name($request)='ValidationFailureDetail') then
               (
               <ns0:messages>
                                <ns0:message>
                                   <ns0:messageCode>{ $defaultFaultCode }</ns0:messageCode>
                                   <ns0:messageDescription>{$request/*}</ns0:messageDescription>
                               </ns0:message>
                              </ns0:messages>
            )            
            else
            (
            if(fn:exists($request//*:errorCode)) then
               (
               <ns0:messages>
                                <ns0:message>
                                   <ns0:messageCode>{ data($request//*:errorCode) }</ns0:messageCode>
                                   <ns0:messageDescription>{data($request//*:reason)}</ns0:messageDescription>
                               </ns0:message>
                           </ns0:messages>
            )
            else
            (            
               <ns0:messages>
                                <ns0:message>
                                   <ns0:messageCode>{ $defaultFaultCode }</ns0:messageCode>
                                   <ns0:messageDescription>System error creating a response. Please contact Biogen systems administrator.</ns0:messageDescription>
                               </ns0:message>
                              </ns0:messages>
                                             )                             
            )
            )
            
            }
     </ns0:submitRequest>
};
declare variable $defaultFaultCode  as xs:string external;
declare variable $request as element(*) external;
declare variable $version as xs:string external;

faultResponse_RequestTransformation($request,
    $version, $defaultFaultCode)